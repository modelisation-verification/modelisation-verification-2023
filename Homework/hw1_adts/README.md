# Semantics, modeling and verification: TP1

---
## Rules

* You must do your homework in your private fork of the course repository.
* You must fill your full name in your GitHub profile.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, the exercises are personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.
* Your source code (and tests) must pass all checks of `swift test`
  without warnings or errors.
---

## Homework
* All homeworks are located in the `homework/` directory.
* There will be a specific subfolder for each homework (e.g. `homework/hw1_adts`). Use it. Do not rename the folder, place your solutions anywhere else, or do other things.
* do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* read the complete instructions **before** starting an assignment
* follow the instructions given to you in the assignments
* The swift compiler will warn you.
  Make sure you don't see any warnings when you compile your code.
  You can use `swift build` to run it.
* For testing, we use [XCTest](https://developer.apple.com/documentation/xctest).
  It is already installed in your environment,
  and can run all the tests within the test folder files using: `swift test`.

---


# Implementing ADTs

## Your task
You have to implement the following **Operators** (see below):

|ADT|Generators|In the library |Operators|
|---|----------|-----------|---------|
|Boolean|```true false```|```Boolean.True() Boolean.False()```|```not(x) and(x,y) or(x,y) eq(x,y)```|
|Nat|```zero succ(Nat)```|```Nat.zero()  Nat.succ(Nat)```|```add(x,y) mul(x,y) pre(x) sub(x,y) div(x,y) mod(x) lt(x,y) gt(x,y) eq(x,y) gcd(x,y) ```|
|Integer|```int(Nat,Nat)```|```Integer.int(Nat,Nat)```|```add(x,y) mul(x,y) sub(x,y) div(x,y) abs(x), normalize(x) lt(x,y) gt(x,y) eq(x,y) sign(x) ```|

You can find all of these ADT in the folder: `Source/ProofKit/adts` (you have `//TODO` comments to guide you).  
At least one operator of each ADT is already implemented as a guidance for you.  

An `Integer` is represented as the subtraction of two `Nat`.
`int(3,1)` is equal to `2`.

This is **highly recommended** to write operations by hand before jumping into the code. If you are not able to write it on a paper, you will not be able to code it.
Therefore, be sure to distinguish if the problem comes from your understanding or the translation into *Swift*.

## Helping information

### Tests

The library you are completed contains a lot of codes.
You do not need to understand everything to use it.
This is **highly recommended** to look at tests to understand how the code will be called and how it can be used !
You can even write your code inside the tests if you want to do your own tests (e.g.: `print`), however, be sure the tests are the same as initially at the end.

### What is a rule ?

A rule is composed of two terms, the left and the right term. When you apply a rule, the left term will be transformed into the right term.  
(Look at the implementation on: `Source/ProofKit/rule.swift`)  
For a `rule`, the function init needs at least two parameters (left and right term), but can have three. The **third parameter is a condition** which decides if the rule can be applied or not (e.g.: `x > 10` which is `Nat.gt(Variable(named: "x"), Nat.n(10))` in the library). By default, this parameter is already initialized to `Boolean.True()` (so rule can be applied).  
However you can have rules which need a change of this condition. (`condition` should always return a Boolean: `Boolean.True()` or `Boolean.False()`)

### Library functions

If you notice, you will have to complete the operations *add_operator* for each operations of ADTs.
To help you understand how this works, look at the signatures below:

*add_operator* (String, Operation): The function takes two arguments, the first one is the name of the generator as a string (e.g.: "true"). The second one is a function which declares that the operation exists and describes a part of the signature.
It allows to create a term that suits to the formal definition.

For instance:
```Swift
// Boolean.True refers to the public static func True
self.add_generator("true", Boolean.True)
...
// Create a function which represents the constant True in the library.
// Therefore, we create a new term which is a "true" value of type bool.
public static func True(_:Term...) -> Term {
  return new_term(Value<Bool>(true), "bool")
}
```

You just need to understand that this second parameter refers to a better way to declare that the operation exists.

*add_operator* (String, Operation, Axioms, Types): Otherwise, when it is not a constant, the first two parameters are the same while **Axioms** describe the semantics of the operations.
**Axioms** are what you have to implement for the TP.
It is based on your theoretical knowledge, so you need to understand how to do it by handwriting before implementing it.
*Types* declare the types of input arguments.

**Axioms** are a list of rules (`[Rule]`):
An example of a typical axiom list:
```Swift
[
  // Rule 1
  Rule(/*Here your left term*/, /*Here your right term*/),
  // Rule 2
  Rule(/*Here your left term*/, /*Here your right term*/),
  // Rule 3 with a condition (condition always return a Boolean)
  Rule(/*Here your left term*/, /*Here your right term*/, /*A term that returns a Boolean term*/),
]
```

Be careful about types: **Bool (Swift built-in type) ≠ Boolean (Boolean type declared in the library)**
Each ADT has its own type in the library, which is different from the built-in types.

To write a term, you need to say explicitly where it comes from (its original type). For instance, if you want to call a boolean operation, you will call `Boolean.yourOperationName(...)`.
Do not forget, each operation is just a function, so it takes no input arguments for constant and as many as needed for the others.
Of course, you can use a *Boolean* term in a *Nat* axiom.

There exists a helper to write a `Nat` term easily and not to write a long sequence of successor.
For instance, `Nat.succ(Nat.succ(Nat.succ(0)))` can be translated as `Nat.n(3)`. You can use directly the built-in type `Int` from Swift to help you with the function `Nat.n(Int)`.

### Precisions about library implementation

- You have to define rules for every case.
- The order of your rules is **important**, when we want to apply a rule, we go through
the rules from top to bottom. (Think of it as a: if, else if, else if, ...)
- You can't use `swift run`on a library. Only commands that work are `swift build` and `swift test`

## How are you evaluated ?

You have 3 ADT to complete the exercise, and each ADT is worth 2 points.
The exercises build on top of each other, meaning that you need to implement `Boolean` in order to use it in `Natural` and `Natural` itself is needed for `Integer`.
