# Semantics, modeling and verification: TP3

---
## Rules

* You must do your homework in your private fork of the course repository.
* You must fill your full name in your GitHub profile.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, the exercises are personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.
* Your source code (and tests) must pass all checks of `swift test`
  without warnings or errors.
---

## Homework
* All homeworks are located in the `homework/` directory.˚
* There will be a specific subfolder for each homework (e.g. `homework/hw3_writing_proofs`). Use it. Do not rename the folder, place your solutions anywhere else, or do other things.
* do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* read the complete instructions **before** starting an assignment
* follow the instructions given to you in the assignments
* The swift compiler will warn you.
  Make sure you don't see any warnings when you compile your code.
  You can use `swift build` to run it.
* For testing, we use [XCTest](https://developer.apple.com/documentation/xctest).
  It is already installed in your environment,
  and can run all the tests within the test folder files using: `swift test`.

---


# Implementing Proofs for ADTs

## Your task: Writing Proofs

For this TP, you have 6 proofs to write (in Swift):
- 3 proofs on Naturals (in file `Tests/TP3Tests/ProofsTests/NatProofsTests.swift`):
  - 0 + s(s(0)) = s(0 + s(0)) (not inductive)
  - s(0) + 0 = s(0 + 0) (not inductive)
  - x + 0 = 0 + x (inductive)
- 3 proofs on Stack (in file `Tests/TP3Tests/ProofsTests/StackProofsTests.swift`):
  - cons(2, pop(cons(1,empty))) = cons(2, empty) (not inductive)
  - top(cons(3, cons(42, empty))) = 4 - 1 (not inductive)
  - (size(cons(n,empty)) > size(empty)) = true (not inductive)

You just need to complete parts where is a `TODO`.
If your proof is correct, test will pass !
You have almost the same library as the last TPs.
Examples are given to help you to understand how it works.

## How to proceed ?

To prove new theorems, you have to use the axioms and the following functions:

- Proof.symmetry(Rule) -> Rule
  - symmetry(Rule(a,b)) => Rule(b,a) (where a and b are terms)
- Proof.transitivity(Rule, Rule) -> Rule
  - r1 = Rule(a,b). r2 = Rule(b,c), transitivity(r1,r2) => Rule(a,c)
- Proof.substitutivity(Operation, [Rule]) -> Rule
  - substitutivity(succ, [Rule(x+0,x)]) -> Rule(succ(x+0), succ(x))
  - substituvity(+, [Rule(x+0,x), Rule(0,0)]) -> Rule(x+0+0,x+0)
- Proof.substitution(Rule, Variable, Term) -> Rule
  - substitution(Rule(x+0,x), x, 0) -> Rule(0+0,0)
- Proof.reflexivity(Term) -> Rule)
  - reflexivity(0) -> Rule(0,0)
  - reflexivity(t) -> Rule(t,t) (where t is a Term)

The `substituvity` is applied on many rules which depends on the operations.
For instance, if your operation requires two terms (such as `+`), you have to give 2 rules. You can refer to the formal definition to understand the formal application.
The `reflexivity` is really helpful to create rules from a term and can be used in addition to `transitivity` (e.g.: Look examples above).

You notice in file `Tests/TP3Tests/ProofsTests/NatProofsTests.swift`:

```Swift
// -- x + 0 = x
let t0 = ADTm["nat"].a("+")[0]
// -- x + s(y) = s(x + y)
let t1 = ADTm["nat"].a("+")[1]
```

and in file `Tests/TP3Tests/ProofsTests/StackProofsTests.swift`:

```Swift
// pop(empty) = empty
let t_pop0 = ADTm["stack"].a("pop")[0]
// pop(cons(n,rest)) = rest
let t_pop1 = ADTm["stack"].a("pop")[1]
// top(cons(n,rest)) = n
let t_top1 = ADTm["stack"].a("top")[1]
// x - 0 = x
let t_sub0 = ADTm["nat"].a("-")[0]
// 0 - x = 0
let t_sub1 = ADTm["nat"].a("-")[1]
// s(x) - s(y) = x - y
let t_sub2 = ADTm["nat"].a("-")[2]
// 0 > x = false
let t_gt0 = ADTm["nat"].a(">")[0]
// s(x) > 0 = true
let t_gt1 = ADTm["nat"].a(">")[1]
// s(x) > s(y) = x > y
let t_gt2 = ADTm["nat"].a(">")[2]
// size(empty) = 0
let t_size0 = ADTm["stack"].a("size")[0]
// size(cons(n,rest)) = s(size(rest))
let t_size1 = ADTm["stack"].a("size")[1]
```

These are theorems based on axioms.
You can apply the different rules of equational theories on these theorems.
Look at examples in the tests to better understand.

### How is writing an induction proof in the library ? (Only needed for inductive proof)

```Swift
func testInduction(){

  // What we want to prove
  let conj = Rule(
    // Conjecture you want to prove
  )

  // Inductive proof

  // Prove initial case
  func zero_proof(t: Rule...)->Rule{
    // TODO HERE
  }

  // Prove induction case
  func succ_proof(t: Rule...)->Rule{
    // TODO HERE
  }

  // Execute your proof and tell you if it is correct.
  do {
    let theorem = try Proof.inductive(conj, Variable(named: "x"), ADTm["nat"], [
      "zero": zero_proof,
      "succ": succ_proof
      ]
    )
    print("Inductive result: \(theorem)")
  }
  catch ProofError.InductionFail {
    print("Induction failed!")
    XCTFail()
  }
  catch {
    XCTFail()
  }

}
```

For the inductive case, you have to write functions `zero_proof`and `succ_proof`.  
You have already axioms / theorems / conjecture that you can use inside these functions.

## Information

**IMPORTANT**:  
- You can only use theorems that are still given in tests.
- You **must not** write your own rules ! The only rules you can create come from the functions of equational theories.
- You have example to help you.
- **Write your proof on paper** before you start coding
- For the inductive proof **ONLY**: The library does not verify if you have the right to make a substitution or not in the conjecture. **Do not make substitution in the conjecture. Proofs will be checked manually**
- **COMMENT EACH** operations that you apply (like given examples). If we cannot understand your code, you can lose up to a point.
- Care order of your rules (symmetry)
- Transitivity of: a = b, b = c -> a = c (Transitivity is often needed)
- You can try to print your rules to help you.

## How are you evaluated ?

You have six proofs to complete the exercise. Each proof is worth 1 point.
We will verify your proof manually. You can pass tests and don't have any points if your proof is not correct.  
 **Lisibility and comments are indispensable**.
 You may lose up one point if this is not the case.
