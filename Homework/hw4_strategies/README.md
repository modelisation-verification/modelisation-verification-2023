# Modélisation et vérification de logiciel: TP4

---
## Rules

* You must do your homework in your private fork of the course repository.
* You must fill your full name in your GitHub profile.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, the exercises are personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.
* Your source code (and tests) must pass all checks of `swift test`
  without warnings or errors.
---

## Homework
* All homeworks are located in the `homework/` directory.
* do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* read the complete instructions **before** starting an assignment
* follow the instructions given to you in the assignments
* The swift compiler will warn you.
  Make sure you don't see any warnings when you compile your code.
  You can use `swift build` to run it.
* For testing, we use [XCTest](https://developer.apple.com/documentation/xctest).
  It is already installed in your environment,
  and can run all the tests within the test folder files using: `swift test`.

---

## Your task:

Complete `TODO` in the `Sources/Homework/Strategy.swift` file.

## Information

- **READ** and **UNDERSTAND** Strategies that you have in the course !
- **READ** `tests` to understand how it works and try to do your own tests. (But don't commit it)
- This assignement is another implementation (simplified) of ADTs. The focus is only on strategies and no longer on axioms or proofs.
- Look at the following Swift idiom to help you:
```Swift
if let x = somethingWhichCanBeNil { ... }
```

## How are you evaluated ?

You have six `TODO` to complete where each of them is worth 1 point.
**Lisibility** and **comment** are always essential, don't forget it !

## How to write Terms

Terms which are already implemented with following operations are:

|         | Generators    | Operations                  |
|---------|:-------------:|:----------------------------|
| Nat     |*zero, succ*  |  *add(Nat,Nat), sub(Nat,Nat), mul(Nat,Nat), eq(Nat,Nat)* |
| Boolean | *true, false* | *not(Boolean), and(Boolean,Boolean), or(Boolean, Boolean)* |
| List\<T>| *empty, cons(T, List\<T>)* | *insert(T, List\<T>), insert(T, List\<T>), concat(List\<T>, List\<T>), contains(T, List\<T>), isEmpty(List\<T>), size(List\<T>)*|

Terms are implemented using Swift Enums.
The aim is to simplify the way to write terms.

Here a list of examples that you can write with the library:

|                | Term  | Translate in the library                   |
|----------------|:------|:-----------------------------------|
| Nat Example 1 | *zero*   | `let t: Nat = .zero`          |
| Nat Example 2 | *succ(zero)*| `let t: Nat = .succ(.zero)`|
| Nat Example 3 | *add(succ(zero), x)* | `let t: Nat = .add(.succ(.zero), .var("x"))`|
| Boolean Example 1 | *true*   | `let t: Boolean = .true`          |
| Boolean Example 2 | *not(true)*| `let t: Boolean = .not(.true)`|
| Boolean Example 3 | *and(or(true, x), true))* | `let t: Boolean = .and(.or(.true,.var("x")), .true)`|
| List Example 1 | *empty*   | `let t: List<Nat> = .empty`|
| List Example 2 | *insert(true, cons(false, empty))*   | `let t: List<Boolean> = .insert(.true, .cons(.false, .empty))`|
| List Example 3 | *concat(cons(0, empty), cons(add(0,0), empty))*   | `let t: List<Nat> = .concat(.cons(.zero, .empty), .cons(.add(.zero, .zero), .empty))`|

Notice that the type `List<T>` is generic, meaning that you can choose any type which is a term.
Thanks to the Swift type inference that gives you the possibility to write: `.succ(.zero)` instead of `Nat.succ(Nat.zero)`.

The library adds the possibility to manipulate variables, which are written as `.var("nameOfTheVariable")`, for all types.
The name of the variable is a simple String in Swift.

If you want to see more examples, you can go directly inside: `Tests/HomeworkTests/HomeworkNatTests.swift`.

To help the final readability, the `CustomStringConvertible` protocol has been added and completed for all types, to have a pretty print for terms.
Try to print terms to see the result !
For example: `print(Boolean.and(.or(.true,.var("x")), .true))
` returns `(true ∨ x) ∧ true`.

## How to evaluate Terms ?

Evaluate terms requires a strategy to reduce all of them.
In this library, you can build your own strategy and apply it to evaluate a term.
For instance, let suppose we have the term: *add(add(x,zero),zero)*.
Should we reduce from inside (*innermost*) or outside (*outermost*) ?
- *Innermost*: *add(sub(x,zero),zero)* --> *add(x,zero)* --> *x*
- *Outermost*: *add(sub(x,zero),zero)* --> *sub(x,zero)* --> *x*

Translated into code:
```Swift
let t: Nat = .add(.add(.var("x"), .zero), .zero)
// Innermost strategy:
// Print: Optional(x)
print(Strategy.eval(t: t, s: .innermost(.axiom))!)
// Outermost strategy
// Print: Optional(x)
print(Strategy.eval(t: t, s: .outermost(.axiom))!)
```

Strategies can fail, for this reason the return result is an *Optional*.
If you are sure that the result is not *nil*, you can force to unwrap it using *!*.

The previous example gives the same answer for both cases, but this will not be the case each time.

For example, *add(add(zero,zero), add(zero,zero))*:
- *Innermost*: *add(add(zero,zero), add(zero,zero))* --> *zero*
- *Outermost*: *add(add(zero,zero), add(zero,zero))* --> *add(zero,zero)*

Strategies can be combined as you wish.
Here a list of all strategies which are implemented:
(t is the term to evaluate)
- **identity**: (Identity)[t] = t (returns the term)
- **fail**: (Fail)[t] = fail
- **axiom**: Rewrite the term with the first axiom that matches
- **sequence**:
  - (s1)[t] = fail => (Sequence(s1,s2))[t] = fail (If the first strategy fails, everything fails)
  - (s1)[t] = t' => (Sequence(s1,s2))[t] = (s2)[t]
- **choice**:
  - (s1)[t] = t' => (Choice(s1,s2))[t] = t'
  - (s1)[t] = fail => (Choice(s1,s2))[t] = (s2)[t] (If the first strategy fails, returns the result of the second)
- **all**:
  - (s)[t1] = t1', ..., (s)[tn] = tn' => (All(s))[f(t1,...,tn)] = f(t1',...,tn') (Rewrite direct subterms of f)
  - ∃i, (s)[ti] = fail => (All(s))[f(t1,...,tn)] = fail (If there exists at least one subterm that fails with the strategy, the whole strategy fails)
  - (All(s))[cst] = cst
- **try**: Try(s) = Choice(s, Identity)
- **innermost**: Innermost(s) = μx.Sequence(All(Innermost(x)), Try(Sequence(s,x))) (Strategy which consists to evaluate all subterms before to evaluate the outer term, and apply it recursively)
- **outermost**: Outermost(s) = μx.Sequence(Try(Sequence(s,x)), All(Innermost(x))) (Same logic that *innermost* but goes from outside to inside)
