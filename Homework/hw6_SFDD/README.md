# Modélisation et vérification de logiciel: TP6

---
## Rules

* You must do your homework in your private fork of the course repository.
* You must fill your full name in your GitHub profile.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, the exercises are personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.
* Your source code (and tests) must pass all checks of `swift test`
  without warnings or errors.
---

## Homework
* All homeworks are located in the `homework/` directory.
* do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* read the complete instructions **before** starting an assignment
* follow the instructions given to you in the assignments
* The swift compiler will warn you.
  Make sure you don't see any warnings when you compile your code.
  You can use `swift build` to run it.
* For testing, we use [XCTest](https://developer.apple.com/documentation/xctest).
  It is already installed in your environment,
  and can run all the tests within the test folder files using: `swift test`.

---

## Your task:

File Tree:

```
- Sources
  - hw6_SFDD
    - Factory.swift
    - SFDD.swift
    - SFDD+Extension.swift
- Tests
  - hw6_SFDDTests
    - hw6_SFDDTests.swift
```

### Complete SFDD homomorphisms (6 points)

You need to complete four functions for SFDD. Look at the course to help you and complete **TODO** in the file `Sources/hw6_SFDD/SFDD.swift`.

You have to complete:
- `contains`
- `union`
- `intersection`
- `subtracting`

**/!\\** `subtracting` operation is not in the course. You have to create the homomorphism to give you the correct behaviour. You have examples in the testing part to help you.
In the code: one = top (⊤) and zero = bot (⊥)


### The Factory

The factory is a class which keeps a trace of every operations that are made.
Its main role is to control that each node (SFDD) which is inserted is unique.
Thanks to a uniquess table - `uniquessTable`in the code - we are able to ensure that each new node is either contained in this set or it is not and it will be added.
This behaviour is represented by the following Swift code:

```Swift
// This is a set of SFDD
var uniquenessTable: Set<SFDD<Key>> = []

...

let (_, result) = self.uniquenessTable.insert(
  SFDD(key: key, take: take, skip: skip, factory: self))
```

First, the `insert` function checks if the element is contained in the `uniquenessTable`. If it is right, it does not insert it and simply returns it.
Otherwise, it adds the new element to the set.
The strength of this approach is based on the pointers.
Indeed, to check if two SFDDs are equals, it is enough to compare their pointers, thanks to the `uniquenessTable` again.

**If you want to create/insert a new node (SFDD), you must use the factory and the makeNode function.**

The function `make` is used to write a SFDD from a family set, however you notice that it also uses the `makeNode` function.

**/!\\** The function `makeNode` in `Sources/hw6_SFDD/Factory.swift` depends on the function union. So, your priority have to be implemented `union` !

## Information

You should:  
- Understand the role of the factory !
- Understand the course, you have almost everything in it.
- **READ** tests to understand how a SFDD is created ! You can create your own tests as long as you do not commit them !

## How are you evaluated ?

Tests are divided into 6 parts:
- `testCount` (0 point)
- `testContains` (1 point)
- `testUnion` (1 point)
- `testIntersection` (1 point)
- `testEquality` (1 point)
- `testSubtracting` (2 points)

**Lisibility** and **comments** are always essential, don't forget it !
