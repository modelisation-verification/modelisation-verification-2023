# Modeling and Verification @ [University of Geneva](http://www.unige.ch)

This repository contains important information about this course.
**Do not forget to [watch](https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023) it** as it will allow us to send notifications for events, such as new exercises, homework, slides or fixes.

## Important Information and Links

* [Page on Gitlab: `https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023`](https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023)
* Courses are Thursday 14:00 - 16:00
* Exercises are Thursday 16:00 - 18:00
* You have free time on Friday 14:00 - 16:00 to work on the course/exercises/homeworks. During this period, the teacher and the assistant do not come. This is personal time for you.
* Team: Prof. Didier Buchs, Damien Morard
* Use Gitlab issues to communicate with us (public)
* For private matters send an email or come see us in our offices:
    * `didier.buchs@unige.ch` (office 217)
    * `damien.morard@unige.ch` (office 216)
* Master 90 credits: During this semester, you will have 6 TPs. If you  are following a Master's program which gives you 90 credits, you only have to do 5 TPs instead of 6.
However, you can decide to do all the TPs.
In this situation, the worst grade will be removed.
* **A TP average of at least 4/6 is required to take the exam.**

## Environment

This course requires the following **mandatory** environment.
We have taken great care to make it as simple as possible.

* [Gitlab](https://gitlab.unige.ch/): a source code hosting platform that we will for the exercises and homework.
Sign in with your unige account (Click on SWITCHaai).

* [MacOS Big Sur or Higher](https://www.apple.com/macos/big-sur/) or [Ubuntu 18.04 LTS 64bits or Higher](https://www.ubuntu.com/download/desktop), if needed in a virtual machine, for instance using [VirtualBox](http://virtualbox.org), or directly with a dual boot.

You also have to:
* **Watch** the [course page](https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023) to get notifications about the course, exercises and homeworks.

<!-- * [Create your own public/private key](https://gitlab.unige.ch/help/ssh/README#generating-a-new-ssh-key-pair) for gitlab !
Do not forget to complete the section [Adding an SSH key to your Gitlab account](https://gitlab.unige.ch/help/ssh/README#adding-an-ssh-key-to-your-gitlab-account)! -->

* Create a **PRIVATE** repository named `modelisation-verification` (**exactly**). It has to be **empty** (no README, no license).
* [Clone the private repository](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) that you have created.

  ```sh
  git clone https://gitlab.unige.ch/YOURUSERNAME/modelisation-verification-2023.git
  ```


* Add the course repository as a remote (to receive updates, TP material, etc).

  ```sh
  cd modelisation-verification

  git remote add course https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023.git
  ```

* [Add as Maintainer](https://docs.gitlab.com/ee/user/project/members/) the user: [`Damien.Morard`](https://gitlab.unige.ch/Damien.Morard) (Damien Morard).

* Comment on Issue [here](https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023/-/issues/1) with your *full name*, *link to Gitlab profile*, *link to private repository*, *email address*

* [Install Swift on your local machine](https://swift.org/getting-started/#installing-swift)
  * Linux: follow instructions in the document
  * MacOS: Install XCode, launch it and accept the user agreement


* [CLion](https://www.jetbrains.com/clion/): a cross plat-form IDE that we will use for Swift.
[Get a free student license](https://www.jetbrains.com/student/) and [download CLion](https://www.jetbrains.com/shop/download/CL/2023200) (select your distribution).

* [Plugin Swift for CLion](https://plugins.jetbrains.com/plugin/8240-swift).


* Become familiar with Swift (see tutorial linked below)
   * Official: https://swift.org/getting-started/
   * Tutorial on Swift by Dimitri Racordon (@kyouko-taiga): https://kyouko-taiga.github.io/swift-thoughts/tutorial/

   > The supported OS versions are macOS 10.13 (High Sierra) and Ubuntu 18.04.
   > You are highly encouraged to use either of those versions.




The environment you installed contains:
* [Git](https://git-scm.com/docs/gittutorial): the tool for source code management;
* [CLion](https://www.jetbrains.com/clion/): the IDE we will use.

Make sure that your [repository is up-to-date](https://help.github.com/articles/syncing-a-fork/) by running frequently:

  ```sh
  git pull course main
  ```

## Rules

* You must do your homework in your private fork of the course repository.
* If for any reason you have trouble with the deadline,
  contact your teacher as soon as possible.
* The assistants must have access to your source code, but nobody else should have.
* Unless **explicitly** stated, the exercises are personal work. No collaboration, joint work or sharing of code will be tolerated. You can however discuss general approaches with your colleagues.
* Your source code (and tests) must pass all checks of `swift test` without warnings or errors. **A code that does not compile or cannot run the tests will have a score of 1.**



## Homework
* All homeworks are located in the `Homework/` directory.
* There will be a specific subfolder for each homework (e.g. `Homework/hw1_adts`). Use it. Do not rename the folder, place your solutions anywhere else, or do other things.
* Do **not rename** any files, variables, functions, classes, ... unless you are instructed to do so!
* Read the complete instructions **before** starting an assignment
* Follow the instructions given to you in the assignments
* The swift compiler will warn you.
  Make sure you don't see any warnings when you compile your code.
  You can use `build` to run it in CLion.
* For testing in the IDE, everything is explained [here](https://www.jetbrains.com/help/clion/swift.html).
You can also test your code in the terminal with the command: `swift test` (you have to be in the Package.swift directory).
* You have to respect [Swift best practice](https://medium.com/@spmandrus/basics-swift-best-practices-175c21631aad) ! Otherwise, the TP's grade will be decreased by 0.5.
* You have to comment your code ! Explain quickly the logic of what you are doing. One or two line are often enough ! Otherwise, the TP's grade will be decreased by 0.5.


### Homework Deadlines
You have until 23:59:59 on these dates to **push** your solutions to *your* (private) repository.
Generally: No late submissions, no extensions, no exceptions, no dogs eating homework.
If you are in an unfortunate circumstance where you do need an extension, tell us beforehand.
(Sending an email two hours before the deadline is *not* beforehand).

| No.  |     1     |    2     |    3      |    4     |    5     |    6     |
| ---- | --------- | -------- | --------- | -------- | -------- | -------- |
| Date | 11. Oct.  | 25. Oct. | 8. Nov.  | 22. Nov. | 6. Dec.  | 20. Dec. |

### Tokens (Bonus)

For the whole semester, you have 2 tokens to give you extra times for your TPs.
One token equals an additional 24 hours to complete your TP.
A token is automatically consumed if you push your work after the deadline (even for a second) !

### Evaluation

 * Your final TP grade is the average of all of them.
 * Here is an example of the calculation.

   | TP1 | TP2 | TP3 | TP4 | TP5 | TP6 |
   | --- | --- | --- | --- | --- | --- |
   |  4  |  5  |  4  |  5  |  6  |  5  |

   Final TP grade = (4 + 5 + 4 + 5 + 6 + 5) / 6 = 4.85

### Getting Help

You should be old and smart enough to find the solutions to most problems by yourself. I.e.:  
*If you encounter any problems, solve them!*

In case there is a problem you cannot solve, here is the order of escalation:
  1. Google is your friend. (response time < 1 sec.)
  2. Read the manual. (response time < 1 min.)
  3. Ask a friend/colleague. (response time < 30 mins.)
  4. Stackoverflow. [Learn how](https://stackoverflow.com/help/how-to-ask) (response time < 12 hrs.)
  5. Course assistants. (response time < 2 days.)
  6. Professor. (response time ???)


### Homework #0 (unmarked)

**Deadline 27.9.2023**

Make your environment work!  
Specifically:
* [ ] did you `watch` this repo
* [ ] did you create your own **PRIVATE** repo, set this repo as a remote (follow the description above)
* [ ] did you give @damdamo as collaborators
* [ ] did you assert that you can pull from this repository and push into your private one (`git pull course main;git push`)
* [ ] did you install swift?
* [ ] did you install CLion?
* [ ] did you check if can you create a swift project?
* [ ] did you check that your swift project compiles?
* [ ] did you check that you can test the swift project?
* [ ] did you read the [Swift tutorial](https://kyouko-taiga.github.io/swift-thoughts/tutorial/)?
* [ ] did you reply to Issue [here](https://gitlab.unige.ch/modelisation-verification/modelisation-verification-2023/issues/1) with your *full name*, *link to Github profile* (e.g. @damdamo), *link to private repository*, *email address*

<!--
### Homework #1

The source files are located within: `homework/hw1_petrinets/`.
You have to understand the provided code and fill in the missing code (marked with `TODO`).
Do **not** touch the existing code or tests,
but you can add your own tests **in a new file** within the `Tests` folder.

The deadline is 10 october 2017 at 23:59.
We will clone all your repositories using a script,
so make sure that @stklik and @damdamo have read access.

Evaluation will be:

* have you done anything at the deadline?
  (yes: 1 point, no: 0 point)
  * [ ] Done anything
* do you have understood and implemented all the required notions?
  (all: 3 points, none: 0 point)
  * [ ] Reachability graph
  * [ ] Coverability graph
* do you have understood and implemented corners cases of all the required
  notions?
  (all: +2 points, none: 0 point)
  * [ ] Reachability graph
  * [ ] Coverability graph
* do you have correctly written and tested your code?
  (no: -0.5 point for each)
  * [ ] Coding standards
  * [ ] Tests
  * [ ] Code coverage

| Grade |
| ----- |
|       |
 -->
